package main_test

import (
	"bitbucket.org/deltabrot/chi-proxy/handlers"
	"bitbucket.org/deltabrot/chi-proxy/server"
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	//	"strconv"
	"testing"
)

var srv = server.Server{}

func TestMain(m *testing.M) {
	handlers.Srv = &srv

	srv.Initialize()

	handlers.InitializeRoutes()

	dropTable()
	ensureTableExists()

	code := m.Run()

	clearTable()

	os.Exit(code)
}

func TestEmptyTable(t *testing.T) {
	clearTable()

	req, err := http.NewRequest("GET", "/customer", nil)
	if err != nil {
		log.Fatal(err)
	}
	response := executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)

	if body := response.Body.String(); body != "[]\n" {
		t.Errorf("Expected an empty array. Got %s.", body)
	}
}

func TestGetNonExistentCustomer(t *testing.T) {
	clearTable()

	req, _ := http.NewRequest("GET", "/customer/11", nil)
	response := executeRequest(req)

	checkResponseCode(t, http.StatusNotFound, response.Code)

	var m map[string]string
	json.Unmarshal(response.Body.Bytes(), &m)
	if m["error"] != "Customer not found" {
		t.Errorf("Expected the 'error' key of the response to be set to 'Customer not found'. Got '%s'", m["error"])
	}
}

func TestCreateCustomer(t *testing.T) {
	clearTable()

	payload := []byte(`{"first_name":"testname","email":"testname@testmail.test"}`)

	req, _ := http.NewRequest("POST", "/customer", bytes.NewBuffer(payload))
	response := executeRequest(req)

	checkResponseCode(t, http.StatusCreated, response.Code)

	var m map[string]interface{}

	json.Unmarshal(response.Body.Bytes(), &m)

	if m["first_name"] != "testname" {
		t.Errorf("Expected customer first_name to be 'testname'. Got '%v'", m["name"])
	}

	if m["email"] != "testname@testmail.test" {
		t.Errorf("Expected customer email to be 'testname@testmail.test'. Got '%v'", m["email"])
	}

	if m["customer_id"] != 1.0 {
		t.Errorf("Expected customer ID to be '1'. Got '%v'", m["customer_id"])
	}
}

func TestGetCustomer(t *testing.T) {
	clearTable()
	addCustomers(1)

	req, _ := http.NewRequest("GET", "/customer/1", nil)
	response := executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)
}

func TestUpdateCustomer(t *testing.T) {
	clearTable()
	addCustomers(1)

	req, _ := http.NewRequest("GET", "/customer/1", nil)
	response := executeRequest(req)
	var originalCustomer map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &originalCustomer)

	payload := []byte(`{"first_name":"testnameupdated", "email":"testnameupdated@testmail.test"}`)

	req, _ = http.NewRequest("PUT", "/customer/1", bytes.NewBuffer(payload))
	response = executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)

	var m map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &m)

	if m["customer_id"] != originalCustomer["customer_id"] {
		t.Errorf("Expected the ID to remain the same (%v). Got %v", originalCustomer["customer_id"], m["customer_id"])
	}
	if m["first_name"] == originalCustomer["first_name"] {
		t.Errorf("Expected the name to change from '%v' to '%v'. Got '%v'.", originalCustomer["first_name"], m["first_name"], m["first_name"])
	}
	if m["email"] == originalCustomer["email"] {
		t.Errorf("Expected the email to change from '%v' to '%v'. Got '%v'.", originalCustomer["email"], m["email"], m["email"])
	}
}

func TestDeleteCustomer(t *testing.T) {
	clearTable()
	addCustomers(1)

	req, _ := http.NewRequest("GET", "/customer/1", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)

	req, _ = http.NewRequest("DELETE", "/customer/1", nil)
	response = executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)

	req, _ = http.NewRequest("GET", "/customer/1", nil)
	response = executeRequest(req)
	checkResponseCode(t, http.StatusNotFound, response.Code)
}

func addCustomers(count int) {
	if count < 1 {
		count = 1
	}

	for i := 0; i < count; i++ {
		srv.DB.Exec("INSERT INTO customers (first_name) VALUES (?)", "Customer")
	}
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	srv.Router.ServeHTTP(rr, req)

	return rr
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

func ensureTableExists() {
	if _, err := srv.DB.Exec(tableCreationQuery); err != nil {
		log.Fatal(err)
	}
}

func clearTable() {
	srv.DB.Exec("DELETE FROM customers")
	srv.DB.Exec("ALTER TABLE customers AUTO_INCREMENT=1")
}

func dropTable() {
	srv.DB.Exec("DROP TABLE customers")
}

const tableCreationQuery = `CREATE TABLE IF NOT EXISTS customers (
	customer_id int AUTO_INCREMENT,
	first_name varchar(40) NOT NULL,
	last_name varchar(40) DEFAULT '',
	email varchar(120) DEFAULT '',
	create_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	last_update TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (customer_id)
)`
