package server

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/deltabrot/chi-proxy/config"
	"github.com/gorilla/mux"
)

type Server struct {
	Router *mux.Router
	DB     *sql.DB
}

func (server *Server) Initialize() {
	configuration := config.LoadConfig()
	connectionString := fmt.Sprintf("%s:%s@(%s:%s)/%s?parseTime=true", configuration.Username, configuration.Password, configuration.Address, configuration.Port, configuration.Dbname)

	var err error
	server.DB, err = sql.Open("mysql", connectionString)
	if err != nil {
		log.Fatal(err)
	}

	server.Router = mux.NewRouter()
}

func (server *Server) Run(addr string) {
	log.Fatal(http.ListenAndServe(":8000", server.Router))
}
