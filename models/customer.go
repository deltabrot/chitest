package models

import (
	"time"
)

type Customer struct {
	CustomerId  int       `json:"customer_id"`
	FirstName   string    `json:"first_name"`
	LastName    string    `json:"last_name"`
	Email       string    `json:"email"`
	CreatedDate time.Time `json:"create_date,omitempty"`
	UpdatedDate time.Time `json:"last_update,omitempty"`
}
