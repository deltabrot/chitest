package handlers

import (
	"bitbucket.org/deltabrot/chi-proxy/server"
)

var Srv *server.Server

func InitializeRoutes() {
	InitializeCustomerRoutes()
}
