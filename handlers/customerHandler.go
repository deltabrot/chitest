package handlers

import (
	"bitbucket.org/deltabrot/chi-proxy/models"
	"encoding/json"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
)

func InitializeCustomerRoutes() {
	Srv.Router.HandleFunc("/customer", GetCustomers).Methods("GET")
	Srv.Router.HandleFunc("/customer", CreateCustomer).Methods("POST")
	Srv.Router.HandleFunc("/customer/{id}", GetCustomer).Methods("GET")
	Srv.Router.HandleFunc("/customer/{id}", UpdateCustomer).Methods("PUT")
	Srv.Router.HandleFunc("/customer/{id}", DeleteCustomer).Methods("DELETE")
}

func GetCustomers(w http.ResponseWriter, r *http.Request) {
	//var customers []models.Customer
	customers := make([]models.Customer, 0)

	rows, err := Srv.DB.Query("SELECT * FROM customers")
	if err != nil {
		log.Fatal(err)
	} else {
		for rows.Next() {
			var customer models.Customer
			err = rows.Scan(
				&customer.CustomerId,
				&customer.FirstName,
				&customer.LastName,
				&customer.Email,
				&customer.CreatedDate,
				&customer.UpdatedDate,
			)
			if err != nil {
				panic(err.Error())
			}
			customers = append(customers, customer)
		}
	}

	json.NewEncoder(w).Encode(customers)
}

func GetCustomer(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	var customer models.Customer
	err := Srv.DB.QueryRow("SELECT * FROM customers WHERE customer_id=?", params["id"]).Scan(
		&customer.CustomerId,
		&customer.FirstName,
		&customer.LastName,
		&customer.Email,
		&customer.CreatedDate,
		&customer.UpdatedDate,
	)
	if err != nil {
		//log.Fatal(err)
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(models.Error{"Customer not found"})
	} else {
		json.NewEncoder(w).Encode(customer)
	}

}

func CreateCustomer(w http.ResponseWriter, r *http.Request) {
	var customer models.Customer
	_ = json.NewDecoder(r.Body).Decode(&customer)

	_, err := Srv.DB.Exec("INSERT INTO customers (first_name, last_name, email, create_date, last_update) VALUES (?, ?, ?, NOW(), NOW())", customer.FirstName, customer.LastName, customer.Email)
	if err != nil {
		panic(err.Error())
	}
	var customerId int
	err = Srv.DB.QueryRow("SELECT LAST_INSERT_ID()").Scan(&customerId)
	if err != nil {
		panic(err.Error())
	}
	customer.CustomerId = customerId

	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(customer)
}

func UpdateCustomer(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var customer models.Customer
	_ = json.NewDecoder(r.Body).Decode(&customer)

	_, err := Srv.DB.Exec("UPDATE customers SET first_name=?, last_name=?, email=?, last_update=NOW() WHERE customer_id=?", customer.FirstName, customer.LastName, customer.Email, params["id"])
	if err != nil {
		panic(err.Error())
	}

	customer.CustomerId, _ = strconv.Atoi(params["id"])

	json.NewEncoder(w).Encode(customer)
}

func DeleteCustomer(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	_, err := Srv.DB.Exec("DELETE FROM customers WHERE customer_id=?", params["id"])
	if err != nil {
		panic(err.Error())
	}
	json.NewEncoder(w).Encode(1)
}
