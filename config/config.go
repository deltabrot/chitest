package config

import (
	"encoding/json"
	"log"
	"os"
)

type Config struct {
	Username string
	Password string
	Address  string
	Port     string
	Dbname   string
}

func LoadConfig() Config {
	file, err := os.Open("./config.database.json")
	if err != nil {
		log.Fatal(err)
	}
	decoder := json.NewDecoder(file)
	configuration := Config{}
	err = decoder.Decode(&configuration)
	if err != nil {
		log.Fatal(err)
	}
	return configuration
}
