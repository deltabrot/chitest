package main

import (
	"bitbucket.org/deltabrot/chi-proxy/handlers"
	"bitbucket.org/deltabrot/chi-proxy/server"
	_ "github.com/go-sql-driver/mysql"
)

var srv = server.Server{}

func main() {
	handlers.Srv = &srv

	srv.Initialize()

	handlers.InitializeRoutes()

	srv.Run(":8000")
}
